package org.egedede.users

import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.core.Vertx
import io.vertx.core.file.OpenOptions
import io.vertx.core.json.JsonObject
import io.vertx.serviceproxy.ServiceException

/**
 *
 */
class MemberServiceImpl(vertx: Vertx) : MemberService {
    var data: MutableMap<String, String> = mutableMapOf()
    init {
        val fs = vertx.fileSystem()
        val path = "data.json"
        if(!fs.existsBlocking(path)){
            fs.createFileBlocking(path)
        }
//        fs.openBlocking(path, OpenOptions())
        val buffer = fs.readFileBlocking(path)
        if(buffer.length()!=0){
            val datas = JsonObject(buffer.toString())
            for( fieldName in datas.fieldNames()){
                data.put(fieldName, datas.getString(fieldName))
            }
        }
    }

    override fun createUser(document: JsonObject?, resultHandler: Handler<AsyncResult<Boolean>>?) {
        if(document!=null && document.containsKey("login")) {
            if (data.containsKey(document.getString("login"))) {
                resultHandler!!.handle(ServiceException.fail(-1, "login.exists"))
            }else {
                data[document.getString("login")] = document.getString("password")
                resultHandler!!.handle(Future.succeededFuture(true))
            }
        }
        resultHandler!!.handle(ServiceException.fail(-100, "no_data"))
    }

    override fun authenticate(document: JsonObject?, resultHandler: Handler<AsyncResult<Boolean>>?) {
        if(document!=null) {
            val key = document.getString("login")
            if (!data.containsKey(key)) {
                resultHandler!!.handle(ServiceException.fail(-2, "login.does_not_exists"))
            }
            val result = data[key] == document.getString("password")
            resultHandler!!.handle(Future.succeededFuture(result))

        }
        resultHandler!!.handle(ServiceException.fail(-100, "no_data"))
    }



}