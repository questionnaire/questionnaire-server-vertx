package org.egedede.users;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

/**
 *
 */
@ProxyGen
public interface MemberService {

    static MemberService create(Vertx vertx){
        return new MemberServiceImpl(vertx);
    }

    static MemberService createProxy(Vertx vertx, String address){
        return new MemberServiceVertxEBProxy(vertx, address);
    }

    void authenticate(JsonObject document,
        Handler<AsyncResult<Boolean>> resultHandler);

    void createUser(JsonObject document,
        Handler<AsyncResult<Boolean>> resultHandler);
}
