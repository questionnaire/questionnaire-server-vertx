package org.egedede.users

import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.serviceproxy.ServiceBinder
import io.vertx.serviceproxy.ServiceProxyBuilder

/**
 *
 */
class MainVerticle : AbstractVerticle(){

    override fun start(startFuture : Future<Void>) {
        println("Verticle initializing")
        var server = vertx.createHttpServer()

        var router = Router.router(vertx)

        ServiceBinder(vertx)
                .setAddress("login-service")
                .register(MemberService::class.java, MemberServiceImpl(vertx))

        var memberService = ServiceProxyBuilder(vertx)
                .setAddress("login-service")
                .build(MemberService::class.java)
        router.route().handler(BodyHandler.create())
        router.post("/create").handler({routingContext->
            println("Call to create "+routingContext.bodyAsJson)
            memberService.createUser(routingContext.bodyAsJson,{result ->
                if(result.succeeded()){
                    routingContext.response().statusCode=if(result.result()){200}else{401}
                } else {
                    routingContext.response().statusCode=500
                }
                routingContext.response().end()
            })
        })
        router.post("/authenticate").handler { routingContext->
            memberService.authenticate(routingContext.bodyAsJson,{result ->
                if(result.succeeded()){
                    routingContext.response().statusCode=if(result.result()){200}else{401}
                } else {
                    routingContext.response().statusCode=500
                }
                routingContext.response().end()
            })
        }
        router.exceptionHandler { throwable ->
            println("Thrown ${throwable}")

        }
        server.requestHandler({ router.accept(it) }).listen(8080)
        println("Verticle initialized")
    }


}