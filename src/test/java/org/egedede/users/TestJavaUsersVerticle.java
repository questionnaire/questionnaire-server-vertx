package org.egedede.users;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.ext.unit.TestCompletion;
import io.vertx.ext.unit.TestOptions;
import io.vertx.ext.unit.TestSuite;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.unit.report.ReportOptions;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 */
@RunWith(VertxUnitRunner.class)
public class TestJavaUsersVerticle {

    Vertx vertx;
    @Test
    public void testMe(){
        TestSuite suite = TestSuite.create("TestMe");
        suite.before(context -> {
            System.out.println("Initialising test");
            vertx = Vertx.vertx();
            vertx.deployVerticle(new MainVerticle());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        suite.test("createOne",context -> {
            HttpClient httpClient = vertx.createHttpClient();
            HttpClientRequest post = httpClient.post(8080, "http://localhost", "/create",
                    response -> {
                        System.out.println("Received response with status code " + response.statusCode());
                    });
            Handler<Void> endHandler = new Handler<Void>() {
                @Override
                public void handle(Void event) {
                    System.out.println("End handled");
                }
            };
            post.endHandler(endHandler);
            post.end("{login:\"login\", password:\"encoded\"}");
            System.out.println(post);

        });
        TestCompletion console = suite.run(
                new TestOptions().addReporter(
                        new ReportOptions().setTo("console")
                ));
        System.out.println(console.isCompleted());
        System.out.println(console.isSucceeded());
    }
}
